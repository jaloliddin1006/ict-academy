from django import forms
from django.contrib.auth import password_validation
from django.contrib.auth.forms import AuthenticationForm
from django.forms.widgets import PasswordInput, TextInput
from django.contrib.auth.forms import PasswordChangeForm

from .models import User

class CustomAuthForm(AuthenticationForm):
    username = forms.CharField(widget=TextInput(attrs={'class':'form-control', 'placeholder': 'username'}))
    password = forms.CharField(widget=PasswordInput(attrs={'class':'form-control','placeholder':'Password'}))


class UserCreateForm(forms.ModelForm):
   
    username = forms.CharField(widget=TextInput(attrs={'class' : 'form-control', 'placeholder': 'username'}), required=True)
    phone = forms.CharField(widget=TextInput(attrs={'class' : 'form-control', 'placeholder': '+998997980727'}), required=True)
    password = forms.CharField(widget=PasswordInput(attrs={'class' : 'form-control', 'placeholder':'Password'}), required=True)
    confirm_password = forms.CharField(widget=PasswordInput(attrs={'class' : 'form-control', 'placeholder':'Confirm Password'}), required=True)



    class Meta:
        model = User
        fields = ('username', 'phone')
        

    def clean_username(self):
        username = self.cleaned_data['username']
        if User.objects.filter(username=username).exists():
            raise forms.ValidationError("Bu foydalanuvchi nomi ro'yxatdan o'tgan")
        return username
    
        
    def clean_phone(self):   
        phone = self.cleaned_data['phone']
        if User.objects.filter(phone=phone).exists():
            raise forms.ValidationError("Bu foydalanuvchi telefon raqami ro'yxatdan o'tgan")
        return phone

    def clean_confirm_password(self):
        data = self.cleaned_data
        if data['password'] != data['confirm_password']:
            raise forms.ValidationError("Ikkala parolingiz ham bir-biriga teng bo'lishi kerak!")
        return data['confirm_password']
    
    
    def save(self, commit=True):
        user = super().save(commit)
        user.set_password(self.cleaned_data['confirm_password'])
        user.save()

        return user
