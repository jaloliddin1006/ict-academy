from django.contrib import admin
from .models import Blog, BlogCategories, BlogComment, BlogTags
# Register your models here.

admin.site.register(Blog)
admin.site.register(BlogCategories)
admin.site.register(BlogComment)
admin.site.register(BlogTags)