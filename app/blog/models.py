from django.db import models
from app.base.models import BaseModel
from app.accounts.models import User
from ckeditor_uploader.fields import RichTextUploadingField


# Create your models here.

BLOG_STTYLE = (
    ('1', '1'),
    ('2', '2'),
    ('3', '3'),
)


class BlogCategories(BaseModel):
    name = models.CharField(max_length = 255, null=True, blank = True)
    
    def __str__(self):
        return self.name
    
class BlogTags(BaseModel):
    name = models.CharField(max_length = 255, null=True, blank = True)
    
    def __str__(self):
        return self.name
    
    
class Blog(BaseModel):
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)
    title = models.CharField(max_length = 255, null=True, blank = True)
    body = RichTextUploadingField()
    photo1 =  models.ImageField(upload_to='blog/')
    photo2 =  models.ImageField(upload_to='blog/')
    category = models.ForeignKey(BlogCategories, on_delete=models.SET_NULL, null=True, blank=True)
    tag = models.ManyToManyField(BlogTags, blank=True)
    style = models.CharField(max_length = 1, null=True, blank = True, choices = BLOG_STTYLE)
    
    def __str__(self):
        return f"{self.user.first_name} - {self.title}"

class BlogComment(BaseModel):
    blog = models.ForeignKey(Blog, on_delete=models.CASCADE, null=True, blank=True, related_name='commint')
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True, related_name='commint')
    comment = models.TextField(null=True, blank = True)
    parent = models.ForeignKey('self',on_delete=models.CASCADE, blank=True, null=True, related_name='children')
    
    def __str__(self):
        return f'{self.user.first_name} - {self.comment}'