from django.urls import path
from app.blog import views

app_name = 'blog'

urlpatterns = [
    path('', views.BlogView.as_view(), name='blogs'),
    path('<uuid:id>', views.BlogDetailView.as_view(), name='blogs_detail')
]
