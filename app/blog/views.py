from django.shortcuts import render, get_object_or_404
from django.views import View
from .models import Blog, BlogCategories, BlogTags, BlogComment
from django.core.paginator import Paginator
from hitcount.views import HitCountDetailView

# Create your views here.


class BlogView(View):
    def get(self, request, *args, **kwargs):
        
        blog = Blog.objects.all()
        
        page_size = request.GET.get('page_size', 12)
        paginator = Paginator(blog, page_size)

        page_num = request.GET.get('page', 1)
        page_obj = paginator.get_page(page_num)
        
        context = {
            'blog' : page_obj
        }
        return render(request, 'blogs.html', context)
    
    
class BlogDetailView(HitCountDetailView):
    count_hit = True
    def get(self, request, id, *args, **kwargs):
        category = BlogCategories.objects.all()
        tags = BlogTags.objects.all()
        blog = get_object_or_404(Blog, id=id)
        last_blog = Blog.objects.all().order_by('-create_at')
        context = {
            'blog' : blog,
            'last_blog' : last_blog[:3],
            'related_post' : last_blog.filter(category = blog.category),
            'category' : category,
            'tags' : tags
        }
        return render(request, 'blog-details.html', context)