from django.db import models
from app.base.models import BaseModel
# Create your models here.

CATEGORY_STTYLE = (
    ('1', '1'),
    ('2', '2'),
    ('3', '3'),
)




class CourceCategory(BaseModel):
    name = models.CharField(max_length = 255, null=True, blank = True)
    icon = models.ImageField(upload_to='court_categry/', null=True, blank = True)
    style = models.CharField(max_length = 1, null=True, blank = True, choices = CATEGORY_STTYLE)
    
    def __str__(self):
        return self.name