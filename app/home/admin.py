from django.contrib import admin
from .models import HomePage, ClientMind, Mentors, Companies, Project, AboutUs, ContactUs
# Register your models here.

admin.site.register(HomePage)
admin.site.register(ClientMind)
admin.site.register(Mentors)
admin.site.register(Companies)
admin.site.register(AboutUs)
admin.site.register(Project)
admin.site.register(ContactUs)