from django import forms
from .models import ContactUs
from django.forms.widgets import TextInput

class ContactForm(forms.ModelForm):
    full_name = forms.CharField(widget=TextInput(attrs={'class':'form-control', 'placeholder': 'Full Name'}))
    phone = forms.CharField(widget=TextInput(attrs={'class':'form-control', 'placeholder': 'Phone Number'}))
    body = forms.CharField(widget=forms.Textarea(attrs={'class':'form-control', 'placeholder': 'Message'}))
    class Meta:
        model = ContactUs
        fields = '__all__'
