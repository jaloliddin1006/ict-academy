from django.db import models
from app.base.models import BaseModel

class HomePage(BaseModel):
    title = models.TextField(null=True, blank = True)
    description = models.CharField(max_length = 255, null=True, blank = True)
    category_description = models.TextField(null=True, blank = True)
    video_link = models.URLField(null=True, blank = True)
    
    about_description = models.TextField(null=True, blank = True)
    about_photo = models.ImageField(upload_to='about_home/', null=True, blank = True)
    about_video_link = models.URLField(null=True, blank = True)
    about_video_photo = models.ImageField(upload_to='about_video/', null=True, blank = True)
    
    cource_description = models.TextField(null=True, blank = True)
    
    mentor_description = models.TextField(null=True, blank = True)
    
    
    community = models.TextField(null=True, blank = True)
    
    story_description = models.TextField(null=True, blank = True)
    story_video_photo = models.ImageField(upload_to='story_home/', null=True, blank = True)
    story_video_link = models.URLField(null=True, blank = True)
    
    def __str__(self):
        return f'{self.id}'
    
    
class Mentors(BaseModel):
    full_name = models.CharField(max_length = 255, null=True, blank = True)
    photo = models.ImageField(upload_to='mentors/', null=True, blank = True)
    specialty = models.CharField(max_length = 255, null=True, blank = True)
    description = models.TextField(null=True, blank = True)
    
    def __str__(self):
        return self.full_name
    
class ClientMind(BaseModel):
    body = models.TextField(null=True, blank = True)
    full_name = models.CharField(max_length = 255, null=True, blank = True)
    specialty = models.CharField(max_length = 255, null=True, blank = True)
    photo = models.ImageField(upload_to='client/', null=True, blank = True)
    
    def __str__(self):
        return self.full_name
    
    
class Companies(BaseModel):
    name = models.CharField(max_length = 255, null=True, blank = True)
    icon = models.ImageField(upload_to='companies/', null=True, blank = True)
    
    def __str__(self):
        return self.name
    
    
class AboutUs(BaseModel):
    title = models.TextField(null=True, blank = True)
    body = models.TextField(null=True, blank = True)
    photo = models.ImageField(upload_to='about/', null=True, blank = True)
    
    project_title = models.TextField(null=True, blank = True)
    project_body = models.TextField(null=True, blank = True)
    
    def __str__(self):
        return self.title
    
    
class Project(BaseModel):
    title = models.CharField(max_length = 255, null=True, blank = True)
    body = models.TextField(null=True, blank = True)
    photo = models.ImageField(upload_to='project/', null=True, blank = True)
    
    def __str__(self):
        return self.title
    
    
class ContactUs(BaseModel):
    full_name = models.CharField(max_length = 255, null=True, blank = True)
    phone = models.CharField(max_length = 255, null=True, blank = True)
    body = models.TextField(null=True, blank = True)
    
    def __str__(self) -> str:
        return self.full_name